# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0] - 2019-08-03
### Fixed
- Status message display in host apps. With all of the downloading changes in this release, the status message should now be shown whenever the host apps switch to a newly downloaded block list.

### Added
- Automatic periodic user block list updates.
  - This includes **breaking changes** to the User model. Overwriting user state with `User().save()` is sufficient to update any saved instance. However, whitelisted hostnames and block list settings should be taken into account when generating a new state for existing users.
  - Downloads are set to happen approximately 24 hours after the last download when a host app is in the foreground.
  - Newly downloaded block lists are activated on the next user action when a download is available.
- A global download counter. It transmits a value for analytics in a way designed to protect user privacy.
- A whitelisted hostname to the demo host apps. This is for tracking its state during testing.

### Changed
- BlockList downloading. Only the user's active AA state is downloaded.
  - Previously, downloads for all states were being retrieved.
  - If a user changes the AA state and a matching block list has already been downloaded, a new download will not happen.
- BlockList model. It has a new `initiator` property for the purpose of distinguishing automatic downloads.
- Project readme to include more details about user state handling.

## [0.2.1] - 2019-06-21
### Fixed
- A few code errors that were present when compiling without the Swift flag `ABPDEBUG`.
- The source of rules for the host app content blocker extensions. Default block lists are now available from `HostCBExt-common/Resources`.

## [0.2.0] - 2019-06-06
### Added
- A special content blocking rule for debug builds intended to more clearly demonstrate correct operations.
- Tab views in the demo apps to demonstrate handling of multiple web views.
- Configuration, by a property list, for bundled block lists. They are not included by default.
- Configuration, by a property list, for app groups capable of being read outside of the framework. This file defaults to being read from the host app.

### Changed
- Swift version to 5.0.1 including minor code changes as a result.
- Rules processing to now default to loading raw JSON rules. Additional validation is performed if the initial loading fails.
- The project structure. It is now organized under an Xcode workspace, to better support usage by external developers. Code signing requirements were removed for the framework and the demo apps were placed in a separate project.
- Loading of rules in the WebKit rule store. Only the active content blocking rules are loaded. Previously, inactive rules were attempted to be cached in the store but this proved to be unviable.

### Removed
- Default bundled block lists. They are no longer shipped with ABPKit. They are instead downloaded on demand.

### Fixed
- Main thread usage during processing of content blocking rules. Operations are now executed on a separate background operation queue.
- Main thread usage during downloading. Operations are now executed on a separate background operation queue.
- Memory leaks related to `UserBlockListDownloader`. The download session is invalidated when needed.
- Some extraneous operations during rule list processing. They have been eliminated.
- Whitelist handling. It has been revised and tested against all rule list processing changes.

## [0.1.0] - 2018-12-16
### Added
- Initial release. Supports content blocking in WKWebView on iOS and macOS.
