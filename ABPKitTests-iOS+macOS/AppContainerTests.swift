/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

@testable import ABPKit

import RxSwift
import XCTest

class AppContainerTests: XCTestCase {
    func testAppSupportDirectory() {
        do {
            _ = try Persistor.applicationSupportDirectory()
        } catch let err { XCTFail("Application support directory error: \(err).") }
    }

    func testPlistConfig() throws {
        let testName = "Test-ABPKit-Configuration.plist"
        let expectIOS = "group.org.adblockplus.abpkit-ios.testing-only"
        let expectMacOS = "group.org.adblockplus.abpkit-macos.testing-only"
        let cfg = try Configured()
            .byBundledPlist(name: testName, startWith: [AppContainerTests.self], for: ABPKitConfiguration.self)
        XCTAssert(cfg?.appGroupIOS == expectIOS, "Bad iOS group.")
        XCTAssert(cfg?.appGroupMacOS == expectMacOS, "Bad macOS group.")
    }
}
