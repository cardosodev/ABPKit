/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

extension UserBlockListDownloader {
    /// Move a file to a destination. If the file exists, it will be first
    /// removed, if possible. If the operation cannot be completed, the function
    /// will throw an error.
    func moveOrReplaceItem(source: URL,
                           destination: URL?) throws {
        guard let dst = destination else { throw ABPDownloadTaskError.badDestinationURL }
        let mgr = FileManager.default
        var rmErr: Error?
        if mgr.fileExists(atPath: dst.path) {
            do {
                try mgr.removeItem(atPath: dst.path)
            } catch let err { rmErr = err }
        }
        if rmErr == nil {
            try mgr.moveItem(at: source, to: dst)
        } else { throw ABPDownloadTaskError.failedRemoval }
    }

    /// Remove downloads no longer in user **download history** based on a given
    /// user state.
    /// - parameter (User): the user state to be synced.
    public
    func syncDownloads(initiator: DownloadInitiator) -> (User) throws -> User {
        // Strong self is required here.
        return { user in
            let pstr = try Persistor()
            var saved: User!
            switch initiator {
            case .userAction:
                saved = try user.userDownloadsUpdated().saved()
            case .automaticUpdate:
                saved = try user.userDownloadsUpdated().saved()
            }
            self.user = saved // internal state change
            let notInSaved = pstr.jsonFiles()(try pstr.fileEnumeratorForRoot()(Config().containerURL()))
                .filter { url in
                    !((saved.getDownloads()) ?? []).contains {
                        $0.name.addingFileExtension(Constants.rulesExtension) == url.lastPathComponent
                    }
                }
            // Keep the active blocklist:
            let preserved = notInSaved
                .filter {
                    !($0.lastPathComponent == user.getBlockList()?.name.addingFileExtension(Constants.rulesExtension))
                }
            let mgr = FileManager.default
            try preserved.forEach {
                try mgr.removeItem(at: $0)
                self.logWith?($0.path)
            }
            try self.sessionInvalidate()
            return saved
        }
    }
}
