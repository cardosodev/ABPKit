/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

/// Data sent during downloads of block lists.
public
struct BlockListDownloadData {
    let addonName = "abpkit",
    addonVer = ABPActiveVersions.abpkitVersion() ?? "",
    applicationVer = ABPActiveVersions.osVersion(),
    platform = "webkit",
    platformVer = ABPActiveVersions.webkitVersion() ?? ""
    #if os(iOS)
    let application = ABPPlatform.ios.rawValue
    #elseif os(macOS)
    let application = ABPPlatform.macos.rawValue
    #else
    let application = "unknown"
    #endif
    public var queryItems: [URLQueryItem]!

    public
    init(user: User) {
        queryItems = [
            URLQueryItem(name: "addonName", value: addonName),
            URLQueryItem(name: "addonVersion", value: addonVer),
            URLQueryItem(name: "application", value: application),
            URLQueryItem(name: "applicationVersion", value: applicationVer),
            URLQueryItem(name: "platform", value: platform),
            URLQueryItem(name: "platformVersion", value: platformVer),
            URLQueryItem(name: "lastVersion", value: lastVersion()(user)),
            URLQueryItem(name: "downloadCount", value: downloadCountString())
        ]
    }
}

extension BlockListDownloadData {
    /// The initial value of the download counter is being handled in the catch for a not yet
    /// existing counter. This can be better encapsulated the DownloadCounter to go along with a
    /// refactor of the DownloadCounter creation mechanism.
    func downloadCountString() -> String {
        do {
            return try DownloadCounter(fromPersistentStorage: true)?.stringForHTTPRequest() ?? "0"
        } catch { return "0" }
    }

    func lastVersion() -> (User) -> String {
        return { $0.lastVersion ?? "0" }
    }
}
