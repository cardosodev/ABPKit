import RxSwift

enum UserState {
    case observerInitialized
    case downloadsCompleted
    case aaStateChanged
}

/// Provide observer functions for Users.
class UserObservation {
    var userState = BehaviorSubject<UserState>(value: .observerInitialized)
}
