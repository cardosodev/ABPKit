/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift

// FilterList implementations will eventually be removed.

@available(iOS 11.0, macOS 10.13, *)
extension WebKitContentBlocker {
    /// FilterList implementation:
    func concatenatedRules(model: FilterList) -> Observable<(String, Int)> {
        do {
            let url = bundle != nil ? try model.rulesURL(bundle: bundle!) : try model.rulesURL()
            return try concatenatedRules()(RulesHelper().decodedRulesFromURL()(url))
        } catch let err { return .error(err) }
    }

    /// Get the content blocking rules while validating and counting them.
    /// This function will be deprecated in a future version.
    /// - parameter customBundle: For testing and special cases.
    /// - returns: Observable of rules and their count.
    func countedRules(customBundle: Bundle? = nil) -> (User) -> RuleStringAndCount {
        return { user in
            let rhlp = RulesHelper(customBundle: customBundle) // only uses bundle if overridden
            do {
                let url = try rhlp.rulesForUser()(user)
                var total = 0
                return try rhlp.decodedRulesFromURL()(url)
                    .reduce(0) { acc, _ in acc + 1 }
                    .flatMap { cnt -> RawRules in
                        total = cnt
                        return rhlp.rawRulesString()(url)
                    }.flatMap { rawRules -> Observable<(String, Int)> in
                        if let rules = rawRules {
                            return .just((rules, total))
                        } else { return .error(ABPBlockListError.badRulesRaw) }
                    }
            } catch let err { return .error(err) }
        }
    }

    /// Wrapper for rawRulesString.
    func rawRules(customBundle: Bundle? = nil) -> (User) -> RawRules {
        return { user in
            let rhlp = RulesHelper(customBundle: customBundle) // only uses bundle if overridden
            do {
                return try rhlp.rawRulesString()(rhlp.rulesForUser()(user))
            } catch let err { return .error(err) }
        }
    }

    /// Validation result only.
    /// - parameter rules: Raw rules.
    /// - returns: Single with a RulesValidation result.
    func validatedRulesWithRaw(rules: String) -> Single<RulesValidation> {
        return RulesHelper().decodedRulesFromString()(rules)
            .reduce(0) { acc, _ in acc + 1 }
            .asSingle()
            .flatMap { cnt -> Single<RulesValidation> in
                if cnt > Constants.blocklistRulesMax {
                    return .just(RulesValidation(
                        parseSucceeded: true, rulesCounted: cnt, error: ABPBlockListError.ruleCountExceeded))
                }
                return .just(RulesValidation(
                    parseSucceeded: true, rulesCounted: cnt, error: nil))
            }
            .catchError { err in
                return .just(RulesValidation(
                    parseSucceeded: false, rulesCounted: nil, error: err))
            }
    }

    /// String copy is used during WL processing.
    ///
    /// Test rule(s) are injected when ABPDEBUG is defined. Rule count is not
    /// updated. Intended for verifying the host apps.
    func whitelistToRulesAdded(customBundle: Bundle? = nil) -> (User) -> RawRules {
        return { user in
            return RulesBeforeWK(user: user, wkcb: self, customBundle: customBundle)
                .rawRules()
        }
    }

    /// Handles blocklist rules for a user. User white list rule is added here
    /// because "ignore-previous-rules" seems to only apply within the context
    /// of the same rule list.
    func concatenatedRules(user: User,
                           customBundle: Bundle? = nil) -> RuleStringAndCount {
        let rhlp = RulesHelper(customBundle: customBundle) // only uses bundle if overridden
        let withWL: (User) throws -> RuleStringAndCount = { [unowned self] in
            try self.concatenatedRules(customBundle: customBundle)(rhlp.decodedRulesFromURL()(rhlp.rulesForUser()($0))
                .concat(self.whiteListRuleForUser()($0)))
        }
        let withoutWL: (User) throws -> RuleStringAndCount = { [unowned self] in
            try self.concatenatedRules(customBundle: customBundle)(rhlp.decodedRulesFromURL()(rhlp.rulesForUser()($0)))
        }
        do {
            if user.whitelistedDomains?.count ?? 0 > 0 {
                return try withWL(user)
            } else {
                return try withoutWL(user)
            }
        } catch let err { return .error(err) }
    }

    /// Embedding a subscription inside this Observable has yielded the fastest performance for
    /// concatenating rules.
    /// Other methods tried:
    /// 1. flatMap + string append - ~4x slower
    /// 2. reduce - ~10x slower
    /// Returns blocklist string + rules count.
    func concatenatedRules(customBundle: Bundle? = nil) -> (Observable<BlockingRule>) -> Observable<(String, Int)> {
        return { obsRules in
            let rhlp = RulesHelper(customBundle: customBundle) // only uses bundle if overridden
            let encoder = JSONEncoder()
            var all = Constants.blocklistArrayStart
            var cnt = 0
            return .create { observer in
                obsRules
                    .subscribe(onNext: { rule in
                        do {
                            cnt += 1
                            try all += rhlp.ruleToStringWithEncoder(encoder)(rule) + Constants.blocklistRuleSeparator
                        } catch let err { observer.onError(err) }
                    }, onCompleted: {
                        observer.onNext((all.dropLast() + Constants.blocklistArrayEnd, cnt))
                        observer.onCompleted()
                    }).disposed(by: self.bag)
                return Disposables.create()
            }
        }
    }

    /// FilterList implementation:
    /// Clear one or more matching rule lists associated with a filter list model.
    func ruleListClearersForModel() -> (FilterList) -> Observable<String> {
        return { [unowned self] model in
            return self.ruleIdentifiers()
                .flatMap { identifiers -> Observable<String> in
                    guard let ids = identifiers else { return .error(ABPWKRuleStoreError.invalidRuleData) }
                    let obs = ids
                        .filter { $0 == model.name }
                        .map { [unowned self] in self.ruleListClearer()($0) }
                    if obs.count < 1 { return .error(ABPWKRuleStoreError.missingRuleList) }
                    return .concat(obs)
                }
        }
    }

    /// Return clearers for rules in the rule store for a user.
    func ruleListClearersForUser() -> (User) -> Observable<String> {
        return { [unowned self] user in
            return self.ruleIdentifiers()
                .flatMap { identifiers -> Observable<String> in
                    guard let hist = user.blockListHistory,
                        let ids = identifiers else { return .error(ABPWKRuleStoreError.invalidRuleData) }
                    let obs = ids
                        .filter { idr in !(hist.contains { $0.name == idr }) }
                        .map { [unowned self] idr in self.ruleListClearer()(idr) }
                    if obs.count < 1 { return .error(ABPWKRuleStoreError.missingRuleList) }
                    return .concat(obs)
                }
        }
    }

    /// Return clearers for all RLs.
    func ruleListAllClearers() -> Observable<String> {
        return ruleIdentifiers()
            .flatMap { identifiers -> Observable<String> in
                guard let ids = identifiers else { return .error(ABPWKRuleStoreError.invalidRuleData) }
                return .concat(ids.map { [unowned self] in self.ruleListClearer()($0) })
            }
    }

    /// Return clearer for an RL.
    func ruleListClearer() -> (String) -> Observable<String> {
        return { idr in
            return .create { [unowned self] observer in
                self.rulesStore.removeContentRuleList(forIdentifier: idr) { err in
                    if err != nil { observer.onError(err!) }
                    observer.onNext(idr)
                    observer.onCompleted()
                }
                return Disposables.create()
            }
        }
    }

    /// Mutative operation on JSON text to allow appending array elements.
    /// * Performed in place to increase performance.
    /// * Fails after `fail` characters have been evaluated.
    func openJSONArray(_ rules: inout String, _ count: Int = 1, _ fail: Int = 10) throws {
        if count > fail { throw ABPBlockListError.failedJSONArrayOpen }
        if rules.popLast() != Character(Constants.blocklistArrayEnd) {
            try openJSONArray(&rules, count + 1)
        }
    }
}
