/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift

typealias RawRules = Observable<(String?)>
typealias RuleStringAndCount = Observable<(String, Int)>

@available(iOS 11.0, macOS 10.13, *)
protocol WhiteListAddable {
    func withoutWL(customBundle: Bundle?) -> (User, WebKitContentBlocker) -> RawRules
    func withWL(customBundle: Bundle?) -> (User, WebKitContentBlocker) -> RawRules
}

/// For adding a WL rule.
@available(iOS 11.0, macOS 10.13, *)
struct WhiteListAdder {
    let wlAdded: (String, User, WebKitContentBlocker) throws -> String = { rules, user, wkcb in
        var copy = rules
        try wkcb.openJSONArray(&copy)
        #if ABPDEBUG
        return copy
            .appending(Constants.blocklistRuleSeparator)
            .appending(wkcb.hostAppTestRuleString(aaEnabled: user.acceptableAdsInUse()))
            .appending(Constants.blocklistRuleSeparator)
            .appending(try wkcb.whiteListRuleStringForUser()(user))
            .appending(Constants.blocklistArrayEnd)
        #else
        return copy
            .appending(Constants.blocklistRuleSeparator)
            .appending(try wkcb.whiteListRuleStringForUser()(user))
            .appending(Constants.blocklistArrayEnd)
        #endif
    }
}

/// For handling rules before they are loaded into WebKit.
/// * Includes addition of a special debugging rule to distinguish content
///   blocking states.
@available(iOS 11.0, macOS 10.13, *)
enum RulesBeforeWK: WhiteListAddable {
    case withoutWhiteList(User, WebKitContentBlocker, Bundle?)
    case whiteListAdded(User, WebKitContentBlocker, Bundle?)

    init(user: User, wkcb: WebKitContentBlocker, customBundle: Bundle?) {
        if user.whitelistedDomains?.count ?? 0 > 0 {
            self = .whiteListAdded(user, wkcb, customBundle)
        }
        self = .withoutWhiteList(user, wkcb, customBundle)
    }
}

@available(iOS 11.0, macOS 10.13, *)
extension RulesBeforeWK {
    func rawRules() -> RawRules {
        switch self {
        case .whiteListAdded(let user, let wkcb, let bndl):
            return self.withWL(customBundle: bndl)(user, wkcb)
        case .withoutWhiteList(let user, let wkcb, let bndl):
            return self.withoutWL(customBundle: bndl)(user, wkcb)
        }
    }

    func withWL(customBundle: Bundle?) -> (User, WebKitContentBlocker) -> RawRules {
        return { user, wkcb in
            return wkcb.rawRules(customBundle: customBundle)(user)
                .flatMap { raw -> RawRules in
                    guard let rules = raw else { return .error(ABPBlockListError.badRulesRaw) }
                    do {
                        #if ABPDEBUG
                        return try .just(WhiteListAdder().wlAdded(rules, user, wkcb))
                        #else
                        return try .just(WhiteListAdder().wlAdded(rules, user, wkcb))
                        #endif
                    } catch let err { return .error(err) }
                }
        }
    }

    func withoutWL(customBundle: Bundle?) -> (User, WebKitContentBlocker) -> RawRules {
        return { user, wkcb in
            #if ABPDEBUG
            return wkcb.rawRules(customBundle: customBundle)(user)
                .flatMap { raw -> RawRules in
                    guard let rules = raw else { return .error(ABPBlockListError.badRulesRaw) }
                    var copy = rules
                    do {
                        try wkcb.openJSONArray(&copy)
                    } catch let err { return .error(err) }
                    return .just(copy
                        .appending(Constants.blocklistRuleSeparator)
                        .appending(wkcb.hostAppTestRuleString(aaEnabled: user.acceptableAdsInUse()))
                        .appending(Constants.blocklistArrayEnd))
                }
            #else
            return wkcb.rawRules(customBundle: customBundle)(user)
            #endif
        }
    }
}
