/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift
import WebKit

@available(iOS 11.0, macOS 10.13, *)
class WebKitContentBlocker: Loggable {
    typealias LogType = [String]?

    let cfg = Config()
    var bag: DisposeBag!
    var bundle: Bundle?
    var rulesStore: WKContentRuleListStore!
    /// For debugging.
    var logWith: ((LogType) -> Void)?

    init?(logWith: ((LogType) -> Void)? = nil) {
        bag = DisposeBag()
        guard let rulesStore =
            try? WKContentRuleListStore(url: cfg.rulesStoreIdentifier())
        else { return nil }
        self.rulesStore = rulesStore
        self.logWith = logWith
    }

    /// From user's block list, make a rule list and add it to the WK rule store.
    /// - parameter user: State for user
    /// - parameter logCompileTime: For logging rule compile time (optional)
    /// - returns: Observable of a rule list
    func rulesAddedWKStore(user: User,
                           initiator: DownloadInitiator,
                           logCompileTime: Bool = false) -> Observable<WKContentRuleList> {
        return whitelistToRulesAdded()(user)
            .flatMap { [unowned self] result -> Observable<WKContentRuleList> in
                guard let rules = result else { return .error(ABPBlockListError.badRulesRaw) }
                return self.rulesCompiled(user: user, rules: rules, initiator: initiator, logCompileTime: logCompileTime)
            }
            .flatMap { [unowned self] rlst -> Observable<WKContentRuleList> in
                return self.ruleListVerified(userList: user.blockList, ruleList: rlst)
            }
    }

    func whiteListRuleForUser() -> (User) -> Observable<BlockingRule> {
        return { user in
            guard let dmns = user.whitelistedDomains, dmns.count > 0 else { return .error(ABPUserModelError.badDataUser) }
            let userWLRule: (User) -> Observable<BlockingRule> = { user in
                var cbUtil: ContentBlockerUtility!
                do {
                    cbUtil = try ContentBlockerUtility()
                } catch let err { return .error(err) }
                return .just(cbUtil.whiteListRuleForDomains()(dmns))
            }
            return userWLRule(user)
        }
    }

    func whiteListRuleStringForUser() -> (User) throws -> String {
        return { user in
            guard let dmns = user.whitelistedDomains, dmns.count > 0 else { throw ABPUserModelError.badDataUser}
            let userWLRuleString: (User) throws -> String = { user in
                let encoded = try String(
                    data: JSONEncoder()
                        .encode(ContentBlockerUtility()
                            .whiteListRuleForDomains()(dmns)),
                    encoding: Constants.blocklistEncoding)
                if let ruleString = encoded {
                    return ruleString
                } else { throw ABPUserModelError.badDataUser }
            }
            return try userWLRuleString(user)
        }
    }

    /// Check that a user list, in their history, matches a rule list in the WK store.
    /// * This function has a completed different meaning from validated rules.
    /// * IDs may be logged using withIDs.
    func ruleListVerified<U: BlockListable>(userList: U?, ruleList: WKContentRuleList) -> Observable<WKContentRuleList> {
        return ruleIdentifiers()
            .flatMap { [unowned self] ids -> Observable<WKContentRuleList> in
                return .create { [unowned self] observer in
                    if let ulst = userList,
                       ulst.name != ruleList.identifier ||
                       ids?.contains(ulst.name) == false { observer.onError(ABPWKRuleStoreError.invalidRuleData) }
                    self.logWith?(ids)
                    observer.onNext(ruleList)
                    observer.onCompleted()
                    return Disposables.create()
                }
            }
    }

    /// Wrapper for IDs.
    func ruleIdentifiers() -> Observable<[String]?> {
        return .create { [unowned self] observer in
            self.rulesStore
                .getAvailableContentRuleListIdentifiers { ids in
                    observer.onNext(ids)
                    observer.onCompleted()
                }
            return Disposables.create()
        }
    }

    /// Wrapper for rules compile.
    /// Rules should match the user's block list.
    func rulesCompiled(user: User,
                       rules: String,
                       initiator: DownloadInitiator,
                       logCompileTime: Bool = false) -> Observable<WKContentRuleList> {
        switch initiator {
        case .userAction:
            return rulesCompiledForIdentifier(user.blockList?.name, logCompileTime: logCompileTime)(rules)
        case .automaticUpdate:
            return rulesCompiledForIdentifier(UUID().uuidString, logCompileTime: logCompileTime)(rules)
        }
    }

    /// Compile rules with WK.
    /// - parameters:
    ///   - identifier: WK rule list ID.
    ///   - logCompileTime: For compile time logging due to being longest running, CPU intensive process.
    /// - returns: Observable with the WK rule list.
    func rulesCompiledForIdentifier(_ identifier: String?,
                                    logCompileTime: Bool = false) -> (String) -> Observable<WKContentRuleList> {
        return { [unowned self] rules in
            return Observable.create { observer in
                let start = Date() // only for compile time logging
                self.rulesStore
                    .compileContentRuleList(forIdentifier: identifier,
                                            encodedContentRuleList: rules) { list, err in
                    if logCompileTime { log("⏱️ cmpl \(fabs(start.timeIntervalSinceNow)) - (\(identifier as String?))") }
                        guard err == nil else { observer.onError(WKErrorHandler(err!)); return }
                        if list != nil {
                            observer.onNext(list!)
                            observer.onCompleted()
                        }
                        observer.onError(ABPWKRuleStoreError.invalidRuleData)
                    }
                return Disposables.create()
            }.subscribeOn(MainScheduler.asyncInstance)
            // In WebKit, compileContentRuleList was found to be not
            // thread-safe. It requires being called from main even though it
            // runs on a different thread.
        }
    }

    /// Remove rule lists from the WK store that are not meant to be active.
    /// The active BL for the user is maintained.
    ///
    /// Technically, rule lists can be precompiled in the rule store, but the current handling
    /// within ABPKit is not sufficient to manage using that kind of setup.
    ///
    /// Future versions may address coordination of multiple active rule lists.
    ///
    /// This is meant to operate on the user state **after** rules have been loaded into the rule
    /// store. In this way, the rules in the store are always available for use according to the
    /// history in a user state.
    ///
    /// See ABPBlockListUpdater.userBlockListUpdate().
    func syncHistoryRemovers(user: User) -> Observable<Observable<String>> {
        var all: [String]!
        do {
            all = try names()(user.getBlockList().map {[$0]})
        } catch let err { return .error(err) }
        return ruleIdentifiers()
            .flatMap { ids -> Observable<Observable<String>> in
                return .create { observer in
                    // Add item to be removed if identifier in the store is not
                    // in the list of IDs.
                    let observables = ids?.filter { idr in !all.contains { $0 == idr } }
                        .map { [unowned self] in return self.listRemovedFromStore(identifier: $0) }
                        .reduce([]) { $0 + [$1] }
                    // Empty string sent to keep operation chains continuous:
                    if let obs = observables, obs.count > 0 {
                        observer.onNext(.concat(obs))
                    } else { observer.onNext(.just("")) }
                    observer.onCompleted()
                    return Disposables.create()
                }
            }
    }

    /// Additional withID closure for debugging as found useful during testing.
    func listRemovedFromStore(identifier: String, withID: ((String) -> Void)? = nil) -> Observable<String> {
        return .create { [unowned self] observer in
            self.rulesStore
                .removeContentRuleList(forIdentifier: identifier) { err in
                    withID?(identifier)
                    // Remove for identifier operation is complete at this point.
                    if err != nil { observer.onError(err!) }
                    observer.onNext(identifier)
                    observer.onCompleted()
                }
            return Disposables.create()
        }
    }

    /// Based on FilterList. Returns an observable after adding. This function
    /// is now only for reference and testing. The User model should be
    /// preferred over FilterList. IDs may be logged using withIDs.
    func addedWKStoreRules(addList: FilterList) -> Observable<WKContentRuleList> {
        return concatenatedRules(model: addList)
            .flatMap { result -> Observable<WKContentRuleList> in
                return .create { observer in
                    // In WebKit, compileContentRuleList requires access to main
                    // even though it runs on a different thread.
                    DispatchQueue.main.async {
                        self.rulesStore
                            .compileContentRuleList(forIdentifier: addList.name,
                                                    encodedContentRuleList: result.0) { [unowned self] list, err in
                                guard err == nil else { observer.onError(err!); return }
                                self.rulesStore.getAvailableContentRuleListIdentifiers { [unowned self] (ids: [String]?) in
                                    if ids?.contains(addList.name) == false { observer.onError(ABPWKRuleStoreError.missingRuleList) }
                                    self.logWith?(ids)
                                }
                                if let compiled = list {
                                    observer.onNext(compiled)
                                    observer.onCompleted()
                                } else { observer.onError(ABPWKRuleStoreError.invalidRuleData) }
                            }
                    }
                    return Disposables.create()
                }
            }
    }

    private
    func names<U: BlockListable>() -> ([U]?) throws -> [String] {
        return {
            guard let models = $0 else { throw ABPUserModelError.badDataUser }
            return models.map { $0.name }.reduce([]) { $0 + [$1] }
        }
    }
}
