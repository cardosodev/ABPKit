/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift
import WebKit

@available(iOS 11.0, macOS 10.13, *)
/// Used by ABPWebViewBlocker and ABPBlockListUpdater.
struct UserDownloadSyncer {
    /// Update the user's blocklist to a new downloaded blocklist.
    func userUpdatedFromDownloads() -> (User) throws -> User {
        return { user in
            if let blst = user.getBlockList(),
                let match = try UserHistory(user: user).downloadsMatch()(blst.source) {
                return user.blockListSet()(match)
            }
            throw ABPUserModelError.badDownloads
        }
    }

    /// Operates on self.user.
    /// Save happens in UserBlockListDownloader+Persistence.syncDownloads().
    func userSyncedDownloadsSaved(initiator: DownloadInitiator, logFileRemovals: Bool = false) -> (User) throws -> User {
        return { user in
            try UserBlockListDownloader(user: user, logWith: logFileRemovals ? { log("🗑️\($0)") } : nil)
                .syncDownloads(initiator: initiator)(user)
        }
    }
}
