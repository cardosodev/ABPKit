/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift

/// Provides periodic checking and updating of a user's block list.
/// However, this module is not intended to alter a user's state.
///
/// - Only the sharedInstance() reference should be used for this class.
/// - Expiration is set with self.expiration.
/// - Updates are stopped by destroying the instance.
@available(iOS 11.0, macOS 10.13, *)
class ABPBlockListUpdater {
    /// Overrideable function that is used to get the user state to update.
    public var userForUpdate: (() -> User?)!
    /// Default implementation of the function that gets the user state to be updated.
    public static let defaultUserForUpdate: () -> User? = {
        do {
            if let user = try User(fromPersistentStorage: true) {
                return user
            }
        } catch { return nil }
        return nil
    }
    private static var privateSharedInstance: ABPBlockListUpdater?
    static var bag: DisposeBag!
    let scheduler = MainScheduler.asyncInstance
    let expireDivisor: Double = Constants.periodicUpdateDivisor
    private var _expiration: TimeInterval
    var expiration: TimeInterval {
        get {
            return _expiration
        }
        set(interval) {
            _expiration = interval
            period = _expiration / expireDivisor
        }
    }
    var period: TimeInterval
    /// Updater errors are not exported yet.
    var updaterErrors = BehaviorSubject<Error?>(value: nil)
    var updaterRunning = false
    var wkcb: WebKitContentBlocker!

    init() {
        fatalError("Should only be accessed from sharedInstance().")
    }

    /// Allows customization of conditions used to determine whether to update the user's active block
    /// list. This may be useful for situations where a block list download may not be preferred or
    /// available.
    private
    init(expiration: TimeInterval = Constants.defaultFilterListExpiration) {
        ABPBlockListUpdater.bag = DisposeBag()
        _expiration = expiration
        period = expiration / expireDivisor
        userForUpdate = ABPBlockListUpdater.defaultUserForUpdate
        wkcb = WebKitContentBlocker(logWith: { log("📙 store \($0 as [String]?)") })
    }

    /// Destroy the shared instance in memory.
    class func destroy() {
        privateSharedInstance = nil
        bag = nil
    }

    func startUpdating() {
        if !updaterRunning {
            userDownloadsUpdate().disposed(by: ABPBlockListUpdater.bag)
            updaterRunning = true
        }
        #if ABPDEBUG
        updaterErrors.subscribe(onNext: { err in
            guard err != nil else { return }
            // Errors are not currently handled here.
            log("🛑 upd_err \(err as Error?)")
        }).disposed(by: ABPBlockListUpdater.bag)
        #endif
    }

    /// Provide access to the shared instance.
    class func sharedInstance() -> ABPBlockListUpdater {
        guard let shared = privateSharedInstance else {
            privateSharedInstance = ABPBlockListUpdater(expiration: Constants.defaultFilterListExpiration)
            return privateSharedInstance!
        }
        return shared
    }

    /// Return true if an update should proceed due to the last download being expired.
    func userBlockListShouldUpdate() -> (User) -> Bool {
        return { [unowned self] user in
            // Check expiration based on downloads and the user BL:
            return ((user.getDownloads()?.filter { $0.initiator == .automaticUpdate }.first?.dateDownload ??
                (user.getBlockList()?.dateDownload)) ?? Date.distantPast)
                    .map { fabs($0.timeIntervalSinceNow) } ?? 0 >= self.expiration
        }
    }

    /// Start a timer to provide a signal for a periodic update.
    /// Call with ABPBlockListUpdater.sharedInstance().userAfterUpdate().
    /// WARNING: Emits no next event when user for update is nil.
    func userAfterUpdate() -> Observable<User> {
        return Observable<Int>.interval(self.period, scheduler: scheduler)
            .filter { [unowned self] _ in
                if let user = ABPBlockListUpdater.sharedInstance().userForUpdate(),
                    self.userBlockListShouldUpdate()(user) {
                        return true
                    }
                return false
            }
            .flatMap { _ -> Observable<User> in
                if let user = ABPBlockListUpdater.sharedInstance().userForUpdate() {
                    let dler = UserBlockListDownloader(user: user)
                    return dler.userAfterDownloads()(dler.userSourceDownloads(initiator: .automaticUpdate))
                }
                return .empty()
            }
    }

    /// Works with a User state but not intended to automatically replace the user's block list.
    func userDownloadsUpdate() -> Disposable {
        let updater = ABPBlockListUpdater.sharedInstance()
        #if ABPDEBUG
        updater.expiration = Constants.blocklistExpirationDebug
        #endif
        return updater.userAfterUpdate()
            .flatMap { usr -> Observable<User> in
                var userToSave: User!
                do {
                    userToSave = try UserDownloadSyncer().userSyncedDownloadsSaved(initiator: .automaticUpdate)(usr)
                } catch let err { return .error(err) }
                return .just(userToSave)
            }.subscribe(onNext: { userToSave in
                /// The rules do not get added to the content controller until the host app calls
                /// useContentBlocking(). This prevents state mismatches between the user's history
                /// and the rules loaded into the WK rule store.
                ///
                /// It is always the **next call** to useContentBlocking() that activates the new rules
                /// (adding them to the content controller). In this way, even if the history maximum is
                /// exceeded by new downloads, it won't affect the next call.
                do {
                    try userToSave.save()
                } catch let err { self.updaterErrors.onNext(err) }
            }, onError: {
                self.updaterErrors.onNext($0)
            }, onDisposed: {
                // This is the only place where destroy should be called.
                ABPBlockListUpdater.destroy()
            })
    }
}
