/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift
import WebKit

@available(iOS 11.0, macOS 10.13, *)
public
protocol ABPBlockable: class {
    var webView: WKWebView! { get }
}

/// Block ads in a WKWebView for framework adopters.
@available(iOS 11.0, macOS 10.13, *)
public
class ABPWebViewBlocker {
    /// Retrieve the last user state. If none, return the default.
    public let lastUser: () throws -> User = {
        if let user = try User(fromPersistentStorage: true) { return user }
        return try User().saved()
    }
    /// Overridable function allowing specification of the user state that will be used for block list updates.
    public var setUserForBlockListUpdate: (@escaping () -> (User)) -> Void = {
        ABPBlockListUpdater.sharedInstance().userForUpdate = $0
    }
    /// Check if a remote source has not yet been downloaded for the user.
    let remoteNotYetDL: (User) -> Bool? = { user in
        return user.getBlockList().map { blst in
            return SourceHelper().isRemote()(blst.source) &&
                blst.dateDownload == nil &&
                user.acceptableAdsInUse() == (blst.source as? RemoteBlockList)?.hasAcceptableAds()
        }
    }
    public var user: User!
    /// For rule processing:
    lazy var abpQueue: OperationQueue = {
        var queue = OperationQueue()
        queue.name = Constants.queueRules
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    var scheduler: OperationQueueScheduler!
    var bag: DisposeBag!
    unowned var ctrl: WKUserContentController!
    /// Log file removals during user state syncing if true.
    var logFileRemovals = false
    /// For debugging: Don't use remote rules when true.
    var noRemote: Bool!
    var ruleListID: String?
    var wkcb: WebKitContentBlocker!
    weak var host: ABPBlockable!

    /// Uses a given user state.
    public
    init(host: ABPBlockable,
         user: User? = nil,
         noRemote: Bool = false,
         logFileRemovals: Bool = false) throws {
        bag = DisposeBag()
        self.host = host
        self.logFileRemovals = logFileRemovals
        self.noRemote = noRemote
        wkcb = WebKitContentBlocker(logWith: { log("📙 store \($0 as [String]?)") })
        ctrl = host.webView.configuration.userContentController
        if user != nil {
            self.user = user
        } else { self.user = try lastUser() }
        scheduler = OperationQueueScheduler(operationQueue: abpQueue)
        _ = ABPBlockListUpdater.sharedInstance()
        ABPBlockListUpdater.sharedInstance().startUpdating()
    }

    deinit {
        bag = nil
    }

    public
    func loadURLString(_ urlString: String, completion: (URL?, Error?) -> Void) {
        if let url = URL(string: urlString.addingWebProtocol()) {
            DispatchQueue.main.async {
                self.host.webView.load(URLRequest(url: url))
            }
            completion(url, nil)
        } else { completion(nil, ABPWebViewBlockerError.badURL) }
    }

    /// Activate bundled or downloaded rule list, as needed.
    /// * Requires a persisted User to be available.
    /// * Compilation can be forced when a new list will be loaded.
    /// - parameter forceCompile: Rule compilation will happen (optional).
    /// - parameter logCompileTime: For logging rule compile time if true (optional).
    /// - parameter logBlockListSwitchDL: For logging BL switching after downloading (optional).
    /// - parameter completeWith: Handle completion.
    public
    func useContentBlocking(forceCompile: Bool = false,
                            logCompileTime: Bool = false,
                            logBlockListSwitchDL: (() -> Void)? = nil,
                            completeWith: @escaping (Error?) -> Void) {
        var didDL = false
        do {
            user = try lastUser() // load user state
        } catch let err { completeWith(err); return }
        fromExistingOrNewRuleList(forceCompile: forceCompile, logCompileTime: true)
            .takeLast(1) // due to the possibility of multiple removes from the rule store
            .flatMap { [unowned self] _ -> Observable<User> in
                // Call the completion handler to allow a UI update but continue additional processing.
                completeWith(nil)
                let shlp = SourceHelper()
                if let src = shlp.userSourceable(self.user), !shlp.isRemote()(src) && !self.noRemote ||
                   self.remoteNotYetDL(self.user) == true {
                    didDL = true
                    return self.withRemoteBLNewDownloader(self.user.acceptableAdsInUse())
                } else {
                    return .just(self.user)
                }
            }
            .filter { _ in didDL }
            .flatMap { [unowned self] user -> Observable<User> in
                do { // state change to remote BL after DLs
                    self.user = try UserDownloadSyncer().userUpdatedFromDownloads()(user).saved()
                } catch let err { completeWith(err) }
                return .just(self.user)
            }
            .flatMap { user -> Observable<WKContentRuleList?> in
                return self.wkcb.rulesAddedWKStore(user: self.user, initiator: .userAction, logCompileTime: logCompileTime)
                    .flatMap { _ -> Observable<WKContentRuleList?> in
                        return self.blockListableAdded()(user)
                    }
            }
            .takeLast(1) // also handles the possibility of multiple removes
            .subscribeOn(self.scheduler)
            .subscribe(onNext: { _ in
                if didDL {
                    logBlockListSwitchDL?()
                }
            }, onError: { err in
                // If the rules failed to load into WK, isolated handling can happen, for example:
                // if type(of: err) == WKErrorHandler.self { }
                //
                // There is an edge case where matching rule store rules do not exist for any member in
                //
                // 1. User history
                // 2. Downloads
                // 3. Blocklist (active)
                //
                // for the current user state** that is not handled yet. Starting with a new user
                // state should bypass this condition if it occurs.
                completeWith(err)
            }, onCompleted: { [unowned self] in
                do {
                    self.user = try UserDownloadSyncer().userSyncedDownloadsSaved(initiator: .userAction)(self.user)
                } catch let err { completeWith(err) }
                completeWith(nil)
            }, onDisposed: {
                // This subscription will be disposed after completion.
            }).disposed(by: bag)
    }

    /// For extra performance, rules are not validated or counted before loading.
    /// This function provides that service.
    /// - parameter completion: Callback that gives a struct containing rules validation results.
    public
    func validateRules(user: User, completion: @escaping (Result<RulesValidation, Error>) -> Void) {
        let rhlp = RulesHelper()
        do {
            _ = try rhlp.rawRulesString()(rhlp.rulesForUser()(user))
                .map { [unowned self] in
                    if let rules = $0 {
                        self.wkcb.validatedRulesWithRaw(rules: rules)
                            .subscribe {
                                switch $0 {
                                case .success(let rslt):
                                    completion(.success(rslt))
                                case .error(let err):
                                    completion(.failure(err))
                                }
                            }.disposed(by: self.bag)
                    } else { throw ABPBlockListError.badRulesRaw }
                }
        } catch let err { completion(.failure(err)) }
    }

    // swiftlint:disable unused_closure_parameter
    /// Synchronize the WK rule store while returning a given added rule list.
    ///
    /// Optionality of rule list, for this one and all related usages, can be removed once a nil
    /// condition is not used to check remote DLs.
    func historySyncForAdded() -> (WKContentRuleList?) -> Observable<WKContentRuleList?> {
        return { [unowned self] list in
            guard let added = list else { return .just(nil) }
            return self.wkcb.syncHistoryRemovers(user: self.user) // one or more removes
                .flatMap { remove -> Observable<String> in
                    return remove
                }
                .flatMap { removed -> Observable<WKContentRuleList?> in
                    return .just(added)
                }
        }
    }
    // swiftlint:enable unused_closure_parameter

    /// Add rules from user's history or user's blocklist.
    /// If a DL needs to happen, this function returns early. This will be refactored in future
    /// revisions to not be required. The intention is to treat existing lists the same as future
    /// (not yet DL) lists.
    public
    func fromExistingOrNewRuleList(forceCompile: Bool = false,
                                   logCompileTime: Bool = false) -> Observable<WKContentRuleList?> {
        guard let blst = user.blockList else { return .error(ABPUserModelError.badDataUser) }
        if remoteNotYetDL(user) == true { return .just(nil) }
        var existing: BlockList?
        do {
            if let update = matchingUpdateCopy()(user) {
                user = try user.blockListSet()(update).historyUpdated()
            } else { existing = try UserHistory(user: user).downloadsMatch()(blst) }
        } catch let err { return .error(err) }
        // Force compilation of lists due to condition that WK rule store should
        // only have active rules. Not forcing allows re-use of existing rules
        // and non-delayed switching among consumers.
        if !forceCompile && existing != nil {
            // Use existing rules in the store:
            return contentControllerAddBlocklistable()(existing)
                .flatMap { [unowned self] blst -> Observable<WKContentRuleList?> in
                    return self.historySyncForAdded()(blst)
                }
        }
        return self.wkcb.rulesAddedWKStore(user: user, initiator: .userAction, logCompileTime: logCompileTime)
            .flatMap { _ -> Observable<WKContentRuleList?> in
                return self.blockListableAdded()(self.user)
            }
    }

    /// Loads rules into the user content controller.
    func blockListableAdded() -> (User) -> Observable<WKContentRuleList?> {
        return { [unowned self] usr in
            if let blst = usr.getBlockList() {
                return self.contentControllerAddBlocklistable()(blst)
                    .flatMap { [unowned self] added -> Observable<WKContentRuleList?> in
                        return self.historySyncForAdded()(added)
                    }
            } else { return .just(nil) }
        }
    }

    /// Initiate a download when needed.
    /// The user state blocklist is switched to a **placeholder** remote source with the AA state of
    /// the user passed in so that the downloader knows how to proceed. The eventual `BlockList`
    /// resulting from the download will be a different one.
    /// - returns: A user state after downloads.
    public
    func withRemoteBLNewDownloader(_ aaInUse: Bool) -> Observable<User> {
        var user: User!
        do {
            user = try User(
                fromPersistentStorage: true,
                withBlockList: BlockList(
                    withAcceptableAds: aaInUse,
                    source: SourceHelper().remoteSourceForAA()(aaInUse),
                    initiator: .userAction))
        } catch let err { return .error(err) }
        let dler = UserBlockListDownloader(user: user)
        return dler.userAfterDownloads()(dler.userSourceDownloads(initiator: .userAction))
    }

    private
    func matchingUpdateCopy() -> (User) -> BlockList? {
        let asRemoteBL: (BlockListSourceable) -> RemoteBlockList? = { $0 as? RemoteBlockList }
        return { user in
            guard let blst = user.getBlockList() else { return nil }
            var copy = user.getDownloads()?.filter { dld in
                dld.initiator == .automaticUpdate &&
                    dld.dateDownload != nil &&
                    asRemoteBL(blst.source) == asRemoteBL(dld.source) &&
                    user.acceptableAdsInUse() == asRemoteBL(dld.source)?.hasAcceptableAds()
            }.first
            copy?.initiator = .userAction
            return copy
        }
    }
}
