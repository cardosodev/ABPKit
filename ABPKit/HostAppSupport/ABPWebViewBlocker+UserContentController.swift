/*
 * This file is part of Adblock Plus <https://adblockplus.org/>,
 * Copyright (C) 2006-present eyeo GmbH
 *
 * Adblock Plus is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 3 as
 * published by the Free Software Foundation.
 *
 * Adblock Plus is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Adblock Plus.  If not, see <http://www.gnu.org/licenses/>.
 */

import RxSwift
import WebKit

@available(iOS 11.0, macOS 10.13, *)
extension ABPWebViewBlocker {
    /// Add rules for a Blocklistable to the content controller. Return list added.
    /// This is a different operation than adding rules to the WK rule store.
    /// See WebKitContentBlocker.rulesAddedWKStore().
    ///
    /// One fallback is used if there is an error on the given list.
    /// Future versions should handle a failed lookup more gracefully.
    func contentControllerAddBlocklistable<U: BlockListable>() -> (U?) -> Observable<WKContentRuleList?> {
        return { [unowned self] in
            guard let ulst = $0 else { return .empty() }
            return self.clearRuleLists()
                .flatMap { [unowned self] () -> Observable<WKContentRuleList?> in
                    return .create { [unowned self] observer in
                        self.addToContentController(abpList: ulst, observer: observer)
                        // The observer does not seem to be accessible after being passed to the function call.
                        return Disposables.create()
                    }
                }
        }
    }

    private
    func addToContentController<U: BlockListable>(abpList: U?, observer: AnyObserver<WKContentRuleList?>, called: Int = 0) {
        /// Embedded here due to use of generics as the containing function is where the type can be determined.
        let fallback: (User) -> U? = {
            if let hist = $0.getHistory(),
                hist.count > 1 {
                    return hist[1] as? U
                }
            return nil
        }
        self.wkcb.rulesStore
            .lookUpContentRuleList(forIdentifier: abpList?.name) { [unowned self] rlist, err in
                if err != nil && called < 1 {
                    #if ABPDEBUG
                    log("⚠️ using fallback")
                    #endif
                    self.addToContentController(abpList: fallback(self.user), observer: observer, called: 1)
                } else if err != nil { observer.onError(err!) }
                if let list = rlist {
                    self.ctrl.add(list)
                    observer.onNext(list)
                }
                observer.onCompleted()
            }
    }

    /// Remove operations require main thread.
    /// This removal works in concert with the individual removers of syncHistoryRemovers (WebKitContentBlocker).
    private
    func clearRuleLists() -> Observable<Void> {
        return Observable.create { [unowned self] observer in
            self.ctrl.removeAllContentRuleLists()
            observer.onNext(())
            observer.onCompleted()
            return Disposables.create()
        }.subscribeOn(MainScheduler.asyncInstance)
    }

    /// The only adder for the content controller.
    private
    func toContentControllerAdd() -> (WKContentRuleList?) throws -> WKContentRuleList? {
        return { [unowned self] in if $0 != nil { self.ctrl.add($0!); return $0! }; return nil }
    }
}
